import React from 'react';
import Items from "./components/Items/Items";

function App() {
  return (
    <div>
      <Items/>
    </div>
  );
}

export default App;
