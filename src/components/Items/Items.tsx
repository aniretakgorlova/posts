import React, {useCallback, useEffect, useState} from 'react';
import {DataTable} from 'primereact/datatable';
import {Column, ColumnEditorOptions} from "primereact/column";
import {Post} from "../../types";
import {InputText} from 'primereact/inputtext';
import axiosApi from "../../axiosApi";
import "primereact/resources/themes/lara-light-indigo/theme.css";
import "primereact/resources/primereact.min.css";
import "primeicons/primeicons.css";
import './items.css';

const Items = () => {
    const [posts, setPosts] = useState<Post[]>([]);
    const columns = [
        {field: 'userId', header: 'User Id'},
        {field: 'id', header: 'Id'},
        {field: 'title', header: 'Title'},
        {field: 'body', header: 'Body'}
    ];

    const POSTS_URL = 'posts';

    const fetchData = useCallback(async () => {
        const postsResponse = await axiosApi.get<Post[]>(POSTS_URL);
        const posts = postsResponse.data;
        setPosts(posts);
    }, []);

    useEffect(() => {
        fetchData().catch(console.error);
    }, [fetchData]);

    const textEditor = (options: ColumnEditorOptions) => {
        return <InputText type="text" className="input_text" name={options.field} value={options.value}
                          onChange={event => changeTextField(event, options)}/>;
    };

    const updateTextFieldById = (id: number, value: string, fieldName: string) => {
        const postsCopy = posts.map(post => {
            if (post.id === id) {
                return {...post, [fieldName]: value};
            }
            return post;
        });
        setPosts(postsCopy);
    };

    const changeTextField = (event: React.ChangeEvent<HTMLInputElement>, options: ColumnEditorOptions) => {
        const value = event.target.value;
        const fieldName = event.target.name;
        // @ts-ignore
        options.editorCallback(value);
        updateTextFieldById(options.rowData.id, value, fieldName);
    };

    return (
        <div>
            <DataTable value={posts} paginator responsiveLayout="scroll"
                       paginatorTemplate="CurrentPageReport PageLinks RowsPerPageDropdown"
                       currentPageReportTemplate="Showing {first} to {last} of {totalRecords}" rows={10}
                       rowsPerPageOptions={[5, 10, 20, 50]}>
                {columns.map(({field, header}) => {
                    return field === 'title' ?
                        <Column className={`column ${field}-column`} key={field} field={field} header={header} editor={(options) => textEditor(options)}/> :
                        <Column className={`column ${field}-column`} key={field} field={field} header={header}/>;
                })}
            </DataTable>
        </div>
    );
};

export default Items;